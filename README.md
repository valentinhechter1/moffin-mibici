# 

## Run the app local

## With docker

```bash
# Build docker image
$ docker-compose build

# Run the docker
$ docker-compose up -d

# See docker logs
$ docker-compose logs -f
```

## Without docker

```bash
# Install dependencies
$ yarn

# Serve with hot reload at localhost:6543
$ yarn dev

# Build for production and launch server
$ yarn build
$ yarn start:prod

# Generate static project
$ yarn generate
```

## Without docker

<!-- Routes  -->

<!-- Get all stations -->
# /stations/ 

<!-- Get nearer stations -->
# /stations/nearer?latitude=&longitude=&distance= 
