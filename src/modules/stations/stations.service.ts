import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client'
import { StationsDto } from 'src/models/stations.dto';


@Injectable()
export class StationsService {
  public nomenclatura = new PrismaClient();

  async getStations(): Promise<Array<any>> {
    const position = await this.nomenclatura.nomenclatura.findMany()
    const stations = []
    position.map(station => stations.push(station.name))

    return stations;
  }

  async gerNearerStation(latitude, longitude, distance): Promise<Array<StationsDto>> {
    const position = await this.nomenclatura.nomenclatura.findMany()

    let list = [];
    let dif = []
    try {
      position.map(pos => list.push({ lat: pos.latitude, lng: pos.longitude, name: pos.name }))
      let point = { lat: latitude, lng: longitude };
      for (let i = 0; i < list.length; ++i) {
        let sum = (Math.abs(point.lat - list[i].lat) * 100) +
          (Math.abs(point.lng - list[i].lng) * 100)
        if (distance > sum) {

          dif.push({
            distance: sum.toFixed(4), name: list[i].name
          })
        }
      }
      dif.sort(((a, b) => a.distance - b.distance));
    }
    catch (err) {
      console.error(err)
    }

    return dif;
  }
}
