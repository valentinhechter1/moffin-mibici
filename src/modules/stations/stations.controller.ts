import { Body, Response, Controller, Get, Post, Put, Query } from '@nestjs/common';
import { StationsService } from './stations.service';

@Controller('stations')
export class StationsController {
  constructor(private readonly stations: StationsService) { }

  @Get('')
  getStations(): Promise<Array<any>> {
    return this.stations.getStations()
  }

  @Get('nearer')
  gerNearerStation(@Response() res, @Query('latitude') latitude: string,
    @Query('longitude') longitude: string, @Query('distance') distance: string) {
    this.stations.gerNearerStation(latitude, longitude, distance).then((stations) => {
      res.send({ data: 'Nearer Stations', stations: stations });
    })
      .catch((stations) => {
        res.send({ data: 'Error cant get nearer stations', stations: stations });
      });
  }
}