FROM node:14 as base

WORKDIR /app

# Add package file
RUN apt update && apt install -y netcat

COPY package.json ./
COPY yarn.lock ./
COPY prisma ./

# Install deps
RUN yarn

# Copy source
COPY . .
# COPY views ./views
# COPY client ./client
COPY tsconfig.json ./

# Build dist
RUN yarn build
RUN yarn prisma generate

# RUN npx prisma generate

# Expose port 3000
EXPOSE 3000
CMD ["yarn", "run", "start:prod"]